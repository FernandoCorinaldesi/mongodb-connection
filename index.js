//iniciamos el gestionador de dependencias con npm init
//instalamos mongodb npm install mongodb
//instalamos nodemon localmente npm install --save-dev nodemon

const { MongoClient } = require('mongodb');//injectamos la driver para la conexion
const fs = require('fs');//
const Json2csvParser = require('json2csv').Parser;//inyectamos el modulo para parsear de json a csv
const opcion = process.argv[2];

async function main() {

    const uri = "mongodb://localhost:27017";
    const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
    try {
        // Coneccion a MongoDB 
        await client.connect();
        console.log('Conexion a la db exitosa');
             

        // llamadas a las funciones
        if(opcion.match("listar")){
            await listDatabases(client);
        }
        else if(opcion.match("convertir")){
            await converToCsv(client);
        }
        else if(opcion.match("mostrar")){
            await showDocuments(client);
        }
        else if(opcion.match("insertar")){
            
            await insertDocument(client,{
                nombre: process.argv[3],
                apellido: process.argv[4],
                edad: process.argv[5]
                })
        }
         else{
            console.log("opcion incorrecta, programa terminado")
        }
        
    } catch (e) {
        console.error(e);
   } finally {
        await client.close();
    }
}
main().catch(console.error);

async function listDatabases(client) {
    databasesList = await client.db().admin().listDatabases();
    console.log("Databases list:");
    //databasesList.databases.forEach(e => console.log(e));
     databasesList.databases.forEach(db => console.log(` - ${db.name}`));
};

async function converToCsv(client) {
  result = await client.db("persona").collection("personas").find().toArray();

  const csvFields = ['_id','nombre','apellido','edad','__v'];
  const json2csvParser = new Json2csvParser({ csvFields });
  const csv = json2csvParser.parse(result);
  console.log("personas en formato csv:");
  console.log(csv);

  fs.writeFile('personas.csv', csv, function(err) {//escribo el archivo csv en la raiz
    if (err) throw err;
    console.log('archivo grabado');
  });
   
};
async function showDocuments(client) {
    result = await client.db("persona").collection("personas").find();
    console.log("lista de personas:");
    await result.forEach((e)=>console.log(e));
  };
async function insertDocument(client,newDoc){
    const result = await client.db("persona").collection("personas").insertOne(newDoc);
    console.log(`Nuevo documento creado con el siguiente id: ${result.insertedId}`);
};